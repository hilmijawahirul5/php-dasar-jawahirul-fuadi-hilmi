<?php
    $nim = "2300018331";
    $nama = "JAWAHIRUL FUADI HILMI";
    $umur = 18;
    $nilai = 87.25;
    $status = true;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Information</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <h2>INFORMASI MAHASISWA</h2>
        <p>NAMA: <?php echo $nama?></p>
        <p>NIM: <?php echo $nim?></p>
        <p>UMUR: <?php echo $umur?></p>
        <p>NILAI: <?php echo $nilai?></p>
        <p>STATUS: <?php
        if($status = true){  
            echo "AKTIF";
        }else {
            echo "TIDAK AKTIF";
        }
        ?></p>
    </div>
</body>
</html>
